package com.cebs.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import com.cebs.beans.Brand;
import com.cebs.beans.Product;
import com.cebs.beans.Stockin;
import com.cebs.beans.User;
import com.cebs.dao.UserDAO;

/**
 * Servlet implementation class Stock
 */
public class Stock extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Stock() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("submit")!=null && request.getParameter("submit").equals("Add Stock"))
		{
			if(!request.getParameter("Product_Price").equals("") && !request.getParameter("Product_Quantity").equals("") && !request.getParameter("Product_Discount").equals("") && !request.getParameter("stockEntry_Date").equals("")) {
			   
		         Stockin stockin=new Stockin();
		         DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		 		 Date date = new Date(); 
		         HttpSession session=request.getSession();
		         User user=(User) session.getAttribute("user");
		         stockin.setPrice(Double.parseDouble(request.getParameter("Product_Price")));
		         stockin.setProductQuantity(Integer.parseInt(request.getParameter("Product_Quantity")));
		         stockin.setDiscountPercentage(Double.parseDouble(request.getParameter("Product_Discount")));
		         stockin.setStockInDate(request.getParameter("stockEntry_Date"));
		         stockin.setUser(user);
		         Product product_type=UserDAO.getInstance().getProductDataById(Integer.parseInt(request.getParameter("Product_Type")));
		         stockin.setProduct(product_type);
		         UserDAO.getInstance().addStockin(stockin);
		         response.sendRedirect("add-stock.jsp?msg=succ");
			}
			else {
				response.sendRedirect("index.jsp?msg=succ");
			}
		}
		else {
			response.sendRedirect("index.jsp?msg=succ");
		}
	}

}
