package com.cebs.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cebs.beans.User;
import com.cebs.dao.UserDAO;
import com.cebs.service.Filter;

/**
 * Servlet implementation class UpdateProfile
 */
public class UpdateProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("submit")!=null && request.getParameter("submit").equals("Update"))
		{
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("user");
			String name[] = request.getParameter("name").split(" ");
			if (name.length == 1) {
				user.setFname(name[0]);
				user.setMname("");
				user.setLname("");
			} else if (name.length == 2) {
				user.setFname(name[0]);
				user.setLname(name[1]);
				user.setMname("");
				user.setMname(null);
			} else if (name.length == 3) {
				user.setFname(name[0]);
				user.setMname(name[1]);
				user.setLname(name[2]);
			}
			user.setPr_email(request.getParameter("pr_email"));
			user.setSc_email(request.getParameter("sc_email"));
			user.setPr_phone(request.getParameter("pr_phone"));
			user.setSc_phone(request.getParameter("sc_phone"));
			user.setGender(request.getParameter("gender"));
			user.setSalt(Filter.createSalt(8));
			user.setPassword(Filter.md5(user.getSalt() + Filter.md5(request.getParameter("password"))));			
			UserDAO.getInstance().updateProfileData(user);
			session.setAttribute("user", user);
			response.sendRedirect("my-profile.jsp");
		}
		else
		{
			response.sendRedirect("index.jsp");
		}
	}
}