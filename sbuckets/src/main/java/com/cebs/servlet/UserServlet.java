package com.cebs.servlet;

import java.io.IOException;
import java.util.Date;

import javax.annotation.processing.Filer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cebs.beans.AuthorizedDevices;
import com.cebs.beans.User;
import com.cebs.beans.UserType;
import com.cebs.dao.UserDAO;
import com.cebs.service.Filter;
import com.cebs.service.Mailer;

/**
 * Servlet implementation class UserServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Check for request is POST or not
		if(request.getMethod().equalsIgnoreCase("POST"))
		{
			//When User click on Register on Register.jsp
			if(request.getParameter("submit")!=null && request.getParameter("submit").equals("Register"))
			{
				if(!request.getParameter("name").equals("") && !request.getParameter("email").equals("") && !request.getParameter("phone").equals("") && !request.getParameter("uname").equals("") && !request.getParameter("password").equals(""))
				{
					User user = new User();
					String name[] = request.getParameter("name").split(" ");
					if(name.length == 1)
					{
						user.setFname(name[0]);
					}
					else if(name.length == 2)
					{
						user.setFname(name[0]);
						user.setLname(name[1]);
					}
					else
					{
						user.setFname(name[0]);
						user.setMname(name[1]);
						user.setLname(name[2]);
					}
					user.setPr_phone(request.getParameter("phone"));
					user.setPr_email(request.getParameter("email"));
					user.setUsername(request.getParameter("uname"));
					user.setGender(request.getParameter("gender"));
					user.setSalt(Filter.createSalt(8));
					user.setPassword(Filter.md5(user.getSalt()+Filter.md5(request.getParameter("password"))));
				
					AuthorizedDevices ad = new AuthorizedDevices();
					ad.setIpAddress(Filter.getClientIpAddr(request));
					ad.setUser(user);
					user.setUsertype(UserDAO.getInstance().getUserType(2));
					
					user = UserDAO.getInstance().registerUser(user);
					if(user!=null)
					{
						UserDAO.getInstance().addAuthorizeDevice(ad);
						String to[] = {request.getParameter("email")};
						String subject = "Welcome To Smart Buckets";
						String body = "Hi "+user.getFname()+"\n\n";
						body += "Welcome to smart buckets please login and complete your profile\n";
						Mailer.sendFromGMail(to, subject, body);
						HttpSession session = request.getSession();
						session.setAttribute("user", user);
						response.sendRedirect("index.jsp");
					}
					else
					{
						response.sendRedirect("Registration?err=dublicate");
					}
				}
				else
				{
					response.sendRedirect("register.jsp?msg=empty");
				}
			}
			else if(request.getParameter("token")!=null && request.getParameter("token").equals("Login"))
			{
				if(!(request.getParameter("uname")!=null && request.getParameter("uname").equals("") && request.getParameter("password")!=null && request.getParameter("password").equals("")))
				{
					String salt = UserDAO.getInstance().getSaltByUname(request.getParameter("uname"));
					if(salt!=null)
					{
						String password = Filter.md5(salt+Filter.md5(request.getParameter("password")));
						User user = UserDAO.getInstance().loginUser(request.getParameter("uname"),password);
						if(user != null)
						{
							AuthorizedDevices ad = UserDAO.getInstance().validateAuthorizeDevice(Filter.getClientIpAddr(request),user);
							if(ad==null)
							{
								UserDAO.getInstance().updateAuthorizedDevice(user);
								ad = new AuthorizedDevices();
								ad.setIpAddress(Filter.getClientIpAddr(request));
								ad.setUser(user);
								UserDAO.getInstance().addAuthorizeDevice(ad);
								String subj = "Security Allert";
								String message = "Hi "+user.getFname()+",\n";
								message += "An anauthorized Device ("+Filter.getClientIpAddr(request)+") has been access you account at IST ("+new Date()+")";
								Mailer.sendFromGMail(new String[]{user.getPr_email()}, subj, message);
							}
							HttpSession session = request.getSession();
							session.setAttribute("user", user);
							if(user.getActive() == 1)
							{
								response.getWriter().print("login success");
							}
							else
							{
								response.getWriter().print("activate account");
							}
						}
						else 
						{
							response.getWriter().print("Invalid Password");
						}
					}
					else
					{
						response.getWriter().print("No Email/User Name exist");
					}
				}
				else
				{
					response.getWriter().print("Fields are empty");
				}
			}
			else
			{
				response.sendRedirect("register.jsp");
			}
		}
		else if(request.getMethod().equalsIgnoreCase("GET"))
		{
			if(request.getParameter("logout")!=null && request.getParameter("logout").equals("true"))
			{
				HttpSession session = request.getSession();
				session.invalidate();
				response.sendRedirect("login.jsp");
			}
			else
			{
				response.sendRedirect("index.jsp");
			}
		}
		else
		{
			response.sendRedirect("login.jsp");
		}
	}

}
