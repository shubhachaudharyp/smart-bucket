package com.cebs.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.cebs.beans.Brand;
import com.cebs.beans.Category;
import com.cebs.beans.Product;
import com.cebs.beans.ProductImages;
import com.cebs.beans.Stockin;
import com.cebs.beans.SubCategory;
import com.cebs.beans.User;
import com.cebs.beans.UserProfileImage;
import com.cebs.dao.UserDAO;
import com.mysql.jdbc.Constants;

/**
 * Servlet implementation class Catalog
 */
@MultipartConfig(maxFileSize = 999999999)
public class Catalog extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Catalog() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (request.getParameter("deleteBrand") != null && request.getParameter("deleteBrand").equals("true")
				&& request.getParameter("brandId") != null) {
			UserDAO.getInstance().deleteBrandById(Integer.parseInt(request.getParameter("brandId")));
			response.sendRedirect("add-brand.jsp");
		}

		else if (request.getParameter("deleteCategory") != null && request.getParameter("deleteCategory").equals("true")
				&& request.getParameter("categoryId") != null) {
			UserDAO.getInstance().deleteCategoryById(Integer.parseInt(request.getParameter("categoryId")));
			response.sendRedirect("add-category.jsp");
		}
		else if (request.getParameter("deleteSubCategory") != null && request.getParameter("deleteSubCategory").equals("true")
				&& request.getParameter("subCategoryId") != null) {
		        UserDAO.getInstance().deleteSubCategoryById(Integer.parseInt(request.getParameter("subCategoryId")));
			response.sendRedirect("add-subcategory.jsp");
		}
		else if (request.getParameter("deleteProduct") != null && request.getParameter("deleteProduct").equals("true")
				&& request.getParameter("producImageId") != null) {
			  UserDAO.getInstance().deleteProductById(Integer.parseInt(request.getParameter("productImageId")));
			response.sendRedirect("add-product.jsp");
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("submit") != null && request.getParameter("submit").equals("Add Brand")) {
			if (!request.getParameter("brand_name").equals("") && !request.getParameter("brand_description").equals("")) {
				Brand brand = new Brand();
				brand.setName(request.getParameter("brand_name"));
				brand.setDiscription(request.getParameter("brand_description"));
				brand.setLogo("logo.jpg");
				Part part = request.getPart("brand_image");
				int errUpload = 0;
				if (part != null && !part.getSubmittedFileName().equals("")) {
					String fileName = part.getSubmittedFileName();
					long fileSize = part.getSize();
					String fileType = part.getContentType();
					if (fileSize >= 2 * 1024 * 1024 && fileSize < 20 * 1024) {
						errUpload = 1;
					}
					if (!fileType.equals("image/png") && !fileType.equals("image/jpg") && !fileType.equals("image/jpeg")
							&& !fileType.equals("image/gif")) {
						errUpload = 2;
					}
					if (errUpload == 0) {
						ServletContext context = request.getServletContext();
						String path = context.getRealPath("\\");
						FileInputStream fin = (FileInputStream) part.getInputStream();
						String name[] = fileName.split("\\.");
						String fileExt = name[name.length - 1];
						String imageName = brand.getName() + (new Random().nextInt(999999999)) + "." + fileExt;
						path = path + "\\uploads\\images\\brandImage\\" + imageName;
						FileOutputStream fout = new FileOutputStream(new File(path));
						int i = 0;
						while ((i = fin.read()) != -1) {
							fout.write(i);
						}
						fout.close();
						fin.close();
						brand.setLogo(imageName);
					} else {
						if (errUpload == 1) {
							response.sendRedirect("add-brand.jsp?msg=sizeErr");
						} else if (errUpload == 2) {
							response.sendRedirect("add-brand.jsp?msg=typeErr");
						}
					}
				}
				if (errUpload == 0) {
					HttpSession session = request.getSession();
					User user = (User) session.getAttribute("user");
					brand.setUser(user);
					UserDAO.getInstance().addBrand(brand);
					response.sendRedirect("add-brand.jsp?msg=succ");
				}
			} else {
				response.sendRedirect("add-brand.jsp?msg=empty");
			}
		} else if (request.getParameter("submit") != null && request.getParameter("submit").equals("Add Category")) {
			if (!request.getParameter("category_name").equals("")) {
				Category category = new Category();
				category.setName(request.getParameter("category_name"));
				category.setLogo("logo.jpg");
				Part part = request.getPart("category_image");
				int errUpload = 0;
				if (part != null && !part.getSubmittedFileName().equals("")) {
					String fileName = part.getSubmittedFileName();
					long fileSize = part.getSize();
					String fileType = part.getContentType();
					if (fileSize >= 2 * 1024 * 1024 && fileSize < 20 * 1024) {
						errUpload = 1;
					}
					if (!fileType.equals("image/png") && !fileType.equals("image/jpg") && !fileType.equals("image/jpeg")
							&& !fileType.equals("image/gif")) {
						errUpload = 2;
					}
					if (errUpload == 0) {
						ServletContext context = request.getServletContext();
						String path = context.getRealPath("\\");
						FileInputStream fin = (FileInputStream) part.getInputStream();
						String name[] = fileName.split("\\.");
						String fileExt = name[name.length - 1];
						String imageName = category.getName() + (new Random().nextInt(999999999)) + "." + fileExt;
						path = path + "\\uploads\\images\\categoryImage\\" + imageName;
						FileOutputStream fout = new FileOutputStream(new File(path));
						int i = 0;
						while ((i = fin.read()) != -1) {
							fout.write(i);
						}
						fout.close();
						fin.close();
						category.setLogo(imageName);
					} else {
						if (errUpload == 1) {
							response.sendRedirect("add-category.jsp?msg=sizeErr");
						} else if (errUpload == 2) {
							response.sendRedirect("add-category.jsp?msg=typeErr");
						}
					}
				}
				if (errUpload == 0) {

					HttpSession session = request.getSession();
					User user = (User) session.getAttribute("user");
					category.setUser(user);
					UserDAO.getInstance().addCategory(category);
					response.sendRedirect("add-category.jsp?msg=succ");
				}
			} else {
				response.sendRedirect("add-category.jsp?msg=empty");
			}
		} else if (request.getParameter("submit") != null && request.getParameter("submit").equals("Add SubCategory")) {
			if (!request.getParameter("subCategory_name").equals("")) {
				SubCategory subCtategory = new SubCategory();
				subCtategory.setName(request.getParameter("subCategory_name"));
				subCtategory.setLogo("logo.jpg");
				Part part = request.getPart("subcategory_image");
				int errUpload = 0;
				if (part != null && !part.getSubmittedFileName().equals("")) {
					String fileName = part.getSubmittedFileName();
					long fileSize = part.getSize();
					String fileType = part.getContentType();
					if (fileSize >= 2 * 1024 * 1024 && fileSize < 20 * 1024) {
						errUpload = 1;
					}
					if (!fileType.equals("image/png") && !fileType.equals("image/jpg") && !fileType.equals("image/jpeg")
							&& !fileType.equals("image/gif")) {
						errUpload = 2;
					}
					if (errUpload == 0) {
						ServletContext context = request.getServletContext();
						String path = context.getRealPath("\\");
						FileInputStream fin = (FileInputStream) part.getInputStream();
						String name[] = fileName.split("\\.");
						String fileExt = name[name.length - 1];
						String imageName = subCtategory.getName() + (new Random().nextInt(999999999)) + "." + fileExt;
						path = path + "\\uploads\\images\\subCategoryImage\\" + imageName;
						FileOutputStream fout = new FileOutputStream(new File(path));
						int i = 0;
						while ((i = fin.read()) != -1) {
							fout.write(i);
						}
						fout.close();
						fin.close();
						subCtategory.setLogo(imageName);
					} else {
						if (errUpload == 1) {
							response.sendRedirect("add-subcategory.jsp?msg=sizeErr");
						} else if (errUpload == 2) {
							response.sendRedirect("add-subcategory.jsp?msg=typeErr");
						}
					}
				}
				if (errUpload == 0) {
					String str1 = request.getParameter("subBrand_Type");
					List<Category> category = UserDAO.getInstance().getCategoryId(str1);
					for (Category ct : category) {
							subCtategory.setCategory(ct);
							UserDAO.getInstance().addSubCategory(subCtategory);
					}
					response.sendRedirect("add-subcategory.jsp?msg=succ");
				}
			}
		}
		else if (request.getParameter("submit") != null && request.getParameter("submit").equals("Add Product")) 
		{
				if (!request.getParameter("Product_name").equals("")) 
				{
					//ServletContext context = request.getServletContext();
					//String path = context.getRealPath("\\");
					//String path =  "\\uploads\\images\\productImage\\";
					//uploadFile(path,request);
					
					Product product = new Product();
					ProductImages productImages =new ProductImages();
					product.setProductName(request.getParameter("Product_name"));
					productImages.setImagePath("logo.jpg");
					Part part = request.getPart("Product_image");
					int errUpload = 0;
					if (part != null && !part.getSubmittedFileName().equals("")) {
						String fileName = part.getSubmittedFileName();
						long fileSize = part.getSize();
						String fileType = part.getContentType();
						if (fileSize >= 2 * 1024 * 1024 && fileSize < 20 * 1024) {
							errUpload = 1;
						}
						if (!fileType.equals("image/png") && !fileType.equals("image/jpg")
								&& !fileType.equals("image/jpeg") && !fileType.equals("image/gif")) {
							errUpload = 2;
						}
						if (errUpload == 0) {
							ServletContext context = request.getServletContext();
							String path = context.getRealPath("\\");
							FileInputStream fin = (FileInputStream) part.getInputStream();
							String name[] = fileName.split("\\.");
							String fileExt = name[name.length - 1];
							String imageName = product.getProductName().split(" ")[0] +  (new Random().nextInt(999999999)) + "."+ fileExt;
							path = path + "\\uploads\\images\\productImage\\" + imageName;
							FileOutputStream fout = new FileOutputStream(new File(path));
							int i = 0;
							while ((i = fin.read()) != -1) {
								fout.write(i);
							}
							fout.close();
							fin.close();
							productImages.setImagePath(imageName);
						} 
						else 
						{
							if (errUpload == 1) {
								response.sendRedirect("add-product.jsp?msg=sizeErr");
							} else if (errUpload == 2) {
								response.sendRedirect("add-product.jsp?msg=typeErr");
							}
						}
					}
					if (errUpload == 0) {
						Brand brand = UserDAO.getInstance().getBrandByID(Integer.parseInt(request.getParameter("BrandProduct_Type")));
					    SubCategory subcategory = UserDAO.getInstance().getSubCategoryByID(Integer.parseInt(request.getParameter("SubCategoryProduct_Type")));	
						product.setBrand(brand);
						product.setSubCategory(subcategory);
						product.setDescription(request.getParameter("Product_description"));
						UserDAO.getInstance().addProduct(product);
						productImages.setProduct(product);
						UserDAO.getInstance().addProductImage(productImages);
						response.sendRedirect("add-product.jsp?msg=succ");
					}
					
				} else {
					response.sendRedirect("add-product.jsp?msg=empty");
				}
		}
	}
	
	
	
	public static List<String> uploadFile(String UPLOAD_DIR, HttpServletRequest request) {
		List<String> fileNames = new ArrayList<String>();
		try {
			List<Part> parts = (List<Part>) request.getParts();
			for (Part part : parts) {
				System.out.println("\n\n\n\n\n\n\n\n\ntesting");
				if (part.getName().equalsIgnoreCase("photos")) {
					String fileName = getFileName(part);
					fileNames.add(fileName);
					String applicationPath = request.getServletContext().getRealPath("");
					String basePath = applicationPath + File.separator + UPLOAD_DIR + File.separator;
					System.out.print("\n\n\n"+basePath+fileName);
					InputStream inputStream = null;
					OutputStream outputStream = null;
					try {
						File outputFilePath = new File(basePath + fileName);
						inputStream = part.getInputStream();
						outputStream = new FileOutputStream(outputFilePath);
						int read = 0;
						final byte[] bytes = new byte[1024];
						while ((read = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, read);
						}
					} catch (Exception ex) {
						fileName = null;
					} finally {
						if (outputStream != null) {
							outputStream.close();
						}
						if (inputStream != null) {
							inputStream.close();
						}
					}
				}
			}
		} catch (Exception e) {
			fileNames = null;
		}
		return fileNames;
	}

	private static String getFileName(Part part) {
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}
	
	
	
}











