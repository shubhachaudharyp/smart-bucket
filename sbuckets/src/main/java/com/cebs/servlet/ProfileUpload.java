package com.cebs.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.cebs.beans.User;
import com.cebs.beans.UserProfileImage;
import com.cebs.dao.UserDAO;

/**
 * Servlet implementation class ProfileUpload
 */
@MultipartConfig(maxFileSize = 999999999)
public class ProfileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getMethod().equalsIgnoreCase("post"))
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			Part part = request.getPart("profile");  
			String fileName = part.getSubmittedFileName();
			long fileSize = part.getSize();
			String fileType = part.getContentType();
			int errUpload = 0;
			if(fileSize>=2*1024*1024 && fileSize<20*1024)
			{
				errUpload = 1;
			}
			if(!fileType.equals("image/png") && !fileType.equals("image/jpg") && !fileType.equals("image/jpeg") && !fileType.equals("image/gif") )
			{
				errUpload = 2;
			}
			if(errUpload == 0)
			{
				HttpSession session = request.getSession();
				User user = (User)session.getAttribute("user");
				ServletContext context = request.getServletContext();
				String path = context.getRealPath("\\");
				FileInputStream fin = (FileInputStream)part.getInputStream();
				String name[] = fileName.split("\\.");
				String fileExt = name[name.length-1];
				String imageName = user.getFname()+"-"+user.getLname()+(new Random().nextInt(999999999))+"."+fileExt;
				path = path + "\\uploads\\images\\profileImage\\"+imageName;
				FileOutputStream fout = new FileOutputStream(new File(path));
				int i=0;
				while((i = fin.read())!=-1)
				{
					fout.write(i);
				}
				fout.close();
				fin.close();
				UserProfileImage image = new UserProfileImage();
				image.setProfilePath(imageName);
				image.setUser(user);
				UserDAO.getInstance().uploadProfileImage(image);
				out.print("profile uploaded");
			}
			else
			{
				if(errUpload == 1)
				{
					out.print("<p style='color:RED'>File must be larger than 20 KB and less than 2 MB</p>");
				}
				else if(errUpload == 2)
				{
					out.print("<p style='color:RED'>File is not an Image</p>");
				}
			}	
		}
		else
		{
			response.sendRedirect("index.jsp");
		}
	}

}
