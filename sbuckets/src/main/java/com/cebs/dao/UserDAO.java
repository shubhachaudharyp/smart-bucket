package com.cebs.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.Order;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.cebs.beans.AuthorizedDevices;
import com.cebs.beans.Brand;
import com.cebs.beans.Category;
import com.cebs.beans.Product;
import com.cebs.beans.ProductImages;
import com.cebs.beans.Stockin;
import com.cebs.beans.Stockout;
import com.cebs.beans.SubCategory;
import com.cebs.beans.User;
import com.cebs.beans.UserProfileImage;
import com.cebs.beans.UserType;

public class UserDAO {
	private Configuration conf = null;
	private SessionFactory sf = null;
    public static UserDAO dao;
	private UserDAO() {
		// TODO Auto-generated constructor stub
		conf = new Configuration().configure().addAnnotatedClass(User.class).addAnnotatedClass(UserType.class)
				.addAnnotatedClass(AuthorizedDevices.class).addAnnotatedClass(UserProfileImage.class)
				.addAnnotatedClass(Brand.class).addAnnotatedClass(Category.class).addAnnotatedClass(SubCategory.class)
				.addAnnotatedClass(Product.class).addAnnotatedClass(ProductImages.class).addAnnotatedClass(Stockin.class);
		         sf = conf.buildSessionFactory();
	}
	public static UserDAO getInstance(){
		if(dao==null) {
			dao=new UserDAO();
		}
		return dao;
	}

	public void uploadProfileImage(UserProfileImage image) {
		Session session = sf.openSession();
		String hql = "update UserProfileImage set isProfile = 0 where user= :user";
		Query query = session.createQuery(hql);
		query.setParameter("user", image.getUser());
		Transaction tx = session.beginTransaction();
		query.executeUpdate();
		tx.commit();
		session.save(image);
		session.beginTransaction().commit();
		session.close();
	}

	public String getProfileImage(User user) {
		Session session = sf.openSession();
		String hql = "from UserProfileImage where user = :user and isProfile = 1";
		Query query = session.createQuery(hql);
		query.setParameter("user", user);
		UserProfileImage profile = (UserProfileImage) query.uniqueResult();
		String pic = null;
		if (profile == null) {
			pic = user.getGender() + ".png";
		} else {
			pic = profile.getProfilePath();
		}
		session.close();
		return pic;
	}

	public void addBrand(Brand brand) {
		Session session = sf.openSession();
		session.save(brand);
		session.close();
	}

	public List<Brand> getBrandByUser(User user) {
		Session session = sf.openSession();
		String hql = "from Brand where user=:user order by id desc";
		Query query = session.createQuery(hql);
		query.setMaxResults(20);
		query.setParameter("user", user);
		List<Brand> brands = query.list();
		session.close();
		return brands;
	}

	public List<Category> getCategoriesByUser(User user) {

		Session session = sf.openSession();
		String hql = "from Category where user=:user order by id desc";
		Query query = session.createQuery(hql);
		query.setMaxResults(20);
		query.setParameter("user", user);
		List<Category> categories = query.list();
		session.close();
		return categories;

	}

	public void deleteBrandById(int brandId) {
		Session session = sf.openSession();
		session.delete(session.get(Brand.class, brandId));
		session.beginTransaction().commit();
		session.close();
	}

	public void deleteCategoryById(int categoryId) {
		Session session = sf.openSession();
		session.delete(session.get(Category.class, categoryId));
		session.beginTransaction().commit();
		session.close();
	}

	public void addCategory(Category category) {

		Session session = sf.openSession();
		session.save(category);
		session.beginTransaction().commit();
		session.close();
	}

	public UserType getUserType(int typeId) {
		Session session = sf.openSession();
		UserType ut = session.get(UserType.class, typeId);
		session.close();
		return ut;
	}

	public void addAuthorizeDevice(AuthorizedDevices ad) {
		Session session = sf.openSession();
		session.save(ad);
		session.beginTransaction().commit();
		session.close();
	}

	public void updateAuthorizedDevice(User user) {
		Session session = sf.openSession();
		String hql = "update AuthorizedDevices set isLastActive = 1 where user = :user";
		Query query = session.createQuery(hql);
		query.setParameter("user", user);
		Transaction tx = session.beginTransaction();
		query.executeUpdate();
		tx.commit();
		session.close();
	}

	public AuthorizedDevices validateAuthorizeDevice(String ip, User user) {
		Session session = sf.openSession();
		String hql = "from AuthorizedDevices where ipAddress=:ip and user = :user";
		Query query = session.createQuery(hql);
		query.setParameter("ip", ip);
		query.setParameter("user", user);
		AuthorizedDevices ad = (AuthorizedDevices) query.uniqueResult();
		session.close();
		return ad;
	}

	public void addUserType(UserType userType) {
		Session session = sf.openSession();
		session.save(userType);
		session.beginTransaction().commit();
		session.close();
	}

	public User registerUser(User user) {
		Session session = sf.openSession();
		if (session.save(user) != null) {
			session.beginTransaction().commit();
			String sql = "select LAST_INSERT_ID()";
			Query query = session.createSQLQuery(sql);
			BigInteger id = (BigInteger) query.uniqueResult();
			user.setUid(id.intValue());
			user.setActive(1);
			session.close();
			return user;
		}
		session.close();
		return null;
	}

	public User loginUser(String name, String password) {
		Session session = sf.openSession();
		String hql = "from User where (username = :uname or pr_email = :email or pr_phone = :phone) and password = :password";
		Query query = session.createQuery(hql);
		query.setParameter("uname", name);
		query.setParameter("email", name);
		query.setParameter("phone", name);
		query.setParameter("password", password);
		User user = (User) query.uniqueResult();
		session.close();
		return user;
	}

	public String getSaltByUname(String uname) {
		Session session = sf.openSession();
		String hql = "select salt from User where username = :uname or pr_email = :email or pr_phone = :phone";
		Query query = session.createQuery(hql);
		query.setParameter("uname", uname);
		query.setParameter("email", uname);
		query.setParameter("phone", uname);
		String salt = (String) query.uniqueResult();
		session.close();
		return salt;
	}

	public void updateProfileData(User user) {
		Session session = sf.openSession();
		session.update(user);
		session.beginTransaction().commit();
		session.close();
	}

	public List<Category> getCategoryId(String cat_name) {

		Session session = sf.openSession();
		String hql = "from Category where name=:name";
		Query query = session.createQuery(hql);
		query.setParameter("name", cat_name);
		List<Category> categories = query.list();
		session.close();
		return categories;
	}

	public void addSubCategory(SubCategory subCtategory) {
		// TODO Auto-generated method stub
		Session session = sf.openSession();
		session.save(subCtategory);
		session.beginTransaction().commit();
		session.close();
	}

	public List<Brand> getBrandProductById() {

		Session session = sf.openSession();
		List<Brand> brands = session.getSession().createCriteria(Brand.class).list();
		session.close();
		return brands;
	}

	public List<SubCategory> getSubCategoryProductById() {

		Session session = sf.openSession();
		List<SubCategory> subCategories = session.getSession().createCriteria(SubCategory.class).list();
		session.close();
		return subCategories;
	}

	public Brand getBrandByID(int bid)
	{
		Session session = sf.openSession();
		Brand brand = session.get(Brand.class, bid);
		session.close();
		return brand;
	}

	public SubCategory getSubCategoryByID(int sid) 
	{
		Session session = sf.openSession();
		SubCategory scat = session.get(SubCategory.class, sid);
		session.close();
		return scat;
	}

	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		Session session = sf.openSession();
		session.save(product);
		session.beginTransaction().commit();
		session.close();

	}

	public void addProductImage(ProductImages productImages) {
		// TODO Auto-generated method stub
		Session session = sf.openSession();
		session.save(productImages);
		session.beginTransaction().commit();
		session.close();

	}

	public List<Product> getProductList() {

		Session session = sf.openSession();
		List<Product> product = session.getSession().createCriteria(Product.class).list();
		session.close();
		return product;
	}

	public String getProductImageList(Product product) 
	{	
		Session session = sf.openSession();
		String hql = "select imagePath from ProductImages where product = :product and coverPic = 1";
		Query query = session.createQuery(hql);
		query.setParameter("product", product);
		Object obj = (Object)query.uniqueResult();
		session.close();
		return obj.toString();
	}

	public void deleteProductById(int parseInt) {
		// TODO Auto-generated method stub
		Session session = sf.openSession();
		session.delete(session.get(ProductImages.class, parseInt));
		session.beginTransaction().commit();
		session.close();
	}

	public void deleteSubCategoryById(int parseInt) {
		// TODO Auto-generated method stub
		Session session = sf.openSession();
		session.delete(session.get(SubCategory.class, parseInt));
		session.beginTransaction().commit();
		session.close();
		
	}

	public Product getProductDataById(int productID) {
		
		Session session = sf.openSession();
	    Product product=session.get(Product.class, productID);
		session.close();
		return product;
	}

	public void addStockin(Stockin stockin) {
		// TODO Auto-generated method stub
		Session session=sf.openSession();
		session.save(stockin);
		session.beginTransaction().commit();
		session.close();
	}
}