package com.cebs.beans;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="sb_UserType")
public class UserType 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int typeId;
	private String userType;
	private int active;
	@OneToMany(fetch=FetchType.LAZY,mappedBy="usertype")
	private List<User> user;
	public UserType() {
		// TODO Auto-generated constructor stub
		active = 1;
		user = new LinkedList<User>();
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
}
