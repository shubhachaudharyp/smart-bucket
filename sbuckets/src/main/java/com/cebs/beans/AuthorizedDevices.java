package com.cebs.beans;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="sb_AuthorizedDevices")
public class AuthorizedDevices 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int authId;
	private String ipAddress;
	private Date dateOfActive;
	private int isLastActive;
	@ManyToOne
	private User user;
	public AuthorizedDevices() {
		// TODO Auto-generated constructor stub
		dateOfActive = new Date();
		isLastActive = 1;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getDateOfActive() {
		return dateOfActive;
	}

	public void setDateOfActive(Date dateOfActive) {
		this.dateOfActive = dateOfActive;
	}

	public int getIsLastActive() {
		return isLastActive;
	}

	public void setIsLastActive(int isLastActive) {
		this.isLastActive = isLastActive;
	}
	
}
