package com.cebs.beans;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="sb_user")
public class User 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int uid;
	private String fname;
	private String lname;
	private String mname;
	private String gender;
	@Column(unique = true)
	private String pr_email;
	private String sc_email;
	@Column(unique = true)
	private String pr_phone;
	private String sc_phone;
	@Column(unique = true)
	private String username;
	private String salt;
	private String password;
	@ManyToOne
	private UserType usertype;
	@OneToMany(mappedBy = "user")
	private List<AuthorizedDevices> devices;
	@OneToMany(mappedBy = "user" )
	private List<UserProfileImage> profileImages;
	private int active;
	@OneToMany(mappedBy = "user")
	private List<Brand> brand;
	@OneToMany(mappedBy = "user")
	private List<Category> categories;
	@OneToMany(mappedBy = "user")
	private List<Stockin> stockins;
	public User() {
		// TODO Auto-generated constructor stub
		lname = "";
		mname = "";
		sc_email = "";
		sc_phone = "";
		active = 1;
		devices = new LinkedList<AuthorizedDevices>();
		profileImages = new LinkedList<UserProfileImage>();
		brand = new LinkedList<Brand>();
		categories = new LinkedList<Category>();
		stockins=new LinkedList<Stockin>();
	}
	public List<Stockin> getStockins() {
		return stockins;
	}
	public void setStockins(List<Stockin> stockins) {
		this.stockins = stockins;
	}


	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<Brand> getBrand() {
		return brand;
	}

	public void setBrand(List<Brand> brand) {
		this.brand = brand;
	}

	public List<UserProfileImage> getProfileImages() {
		return profileImages;
	}

	public void setProfileImages(List<UserProfileImage> profileImages) {
		this.profileImages = profileImages;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<AuthorizedDevices> getDevices() {
		return devices;
	}

	public void setDevices(List<AuthorizedDevices> devices) {
		this.devices = devices;
	}

	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getPr_email() {
		return pr_email;
	}
	public void setPr_email(String pr_email) {
		this.pr_email = pr_email;
	}
	public String getSc_email() {
		return sc_email;
	}
	public void setSc_email(String sc_email) {
		this.sc_email = sc_email;
	}
	public String getPr_phone() {
		return pr_phone;
	}
	public void setPr_phone(String pr_phone) {
		this.pr_phone = pr_phone;
	}
	public String getSc_phone() {
		return sc_phone;
	}
	public void setSc_phone(String sc_phone) {
		this.sc_phone = sc_phone;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserType getUsertype() {
		return usertype;
	}
	public void setUsertype(UserType usertype) {
		this.usertype = usertype;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
}
