package com.cebs.beans;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="sb_userprofileImage")
public class UserProfileImage 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String profilePath;
	private int isProfile;
	private Date date;
	@ManyToOne
	private User user;
	public UserProfileImage() {
		// TODO Auto-generated constructor stub
		isProfile = 1;
		date = new Date();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProfilePath() {
		return profilePath;
	}
	public void setProfilePath(String profilePath) {
		this.profilePath = profilePath;
	}
	public int getIsProfile() {
		return isProfile;
	}
	public void setIsProfile(int isProfile) {
		this.isProfile = isProfile;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
