<%@page import="com.cebs.beans.Brand"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Smart Buckets | addBrand</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- favicon
		============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Google Fonts
		============================================ -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900"
	rel="stylesheet">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- owl.carousel CSS
		============================================ -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.transitions.css">
<!-- animate CSS
		============================================ -->
<link rel="stylesheet" href="css/animate.css">
<!-- normalize CSS
		============================================ -->
<link rel="stylesheet" href="css/normalize.css">
<!-- meanmenu icon CSS
		============================================ -->
<link rel="stylesheet" href="css/meanmenu.min.css">
<!-- main CSS
		============================================ -->
<link rel="stylesheet" href="css/main.css">
<!-- educate icon CSS
		============================================ -->
<link rel="stylesheet" href="css/educate-custon-icon.css">
<!-- morrisjs CSS
		============================================ -->
<link rel="stylesheet" href="css/morrisjs/morris.css">
<!-- mCustomScrollbar CSS
		============================================ -->
<link rel="stylesheet"
	href="css/scrollbar/jquery.mCustomScrollbar.min.css">
<!-- metisMenu CSS
		============================================ -->
<link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
<link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
<!-- calendar CSS
		============================================ -->
<link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
<link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
<!-- modals CSS
		============================================ -->
<link rel="stylesheet" href="css/modals.css">
<!-- forms CSS
		============================================ -->
<link rel="stylesheet" href="css/form/all-type-forms.css">
<!-- style CSS
		============================================ -->
<link rel="stylesheet" href="style.css">
<!-- responsive CSS
		============================================ -->
<link rel="stylesheet" href="css/responsive.css">
<!-- modernizr JS
		============================================ -->
<script src="js/vendor/modernizr-2.8.3.min.js"></script>
<style>
   .upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.btn {
  border: 2px solid gray;
  color: gray;
  background-color: white;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
</head>
<body>
	
	<%@include file="sidebar.jsp"%>

	<!-- End Left menu area -->
	<!-- Start Welcome area -->
	<div class="all-content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="logo-pro">
						<a href="index.html"><img class="main-logo"
							src="img/logo/logo.png" alt="" /></a>
					</div>
				</div>
			</div>
		</div>
		<%@include file="header.jsp"%>
		<!-- Basic Form Start -->
		<div class="basic-form-area mg-b-15">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="sparkline8-list mt-b-30">
							<div class="sparkline8-hd">
								<div class="main-sparkline8-hd">
									<h1>Brand Information</h1>
								</div>
								<div class="main-sparkline8-hd" id="errMessage">
									<%
										if(request.getParameter("msg")!=null)
										{
											if(request.getParameter("msg").equals("sizeErr"))
											{
												out.print("<h3 style='color:red'>Image must be less than 20MB and greater than 20KB</h3>");
											}
											else if(request.getParameter("msg").equals("typeErr"))
											{
												out.print("<h3 style='color:red'>Image must be JPEG/JPG/PNG/GIF only</h3>");
											}
											else if(request.getParameter("msg").equals("empty"))
											{
												out.print("<h3 style='color:red'>Fields are empty</h3>");
											}
											else if(request.getParameter("msg").equals("succ"))
											{
												out.print("<h3 style='color:green'>Brand Inserted</h3>");
											}
											
										}
									
									%>
								</div>
							</div>
							<div class="sparkline8-graph">
								<div class="basic-login-form-ad">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="basic-login-inner">
												<form action="Catalog" method="post"enctype="multipart/form-data">
													<div class="form-group-inner">
														<label>Brand name</label> <input type="text"
															class="form-control" name="brand_name" id="brandName"
															placeholder="Enter Brand Name" />
													</div>
													<div class="form-group-inner">
														<label>Description</label>
														<textarea rows="4" cols="50" class="form-control"
															name="brand_description" placeholder="Enter description"></textarea>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<div class="basic-login-inner upload-btn-wrapper" >
															<br>
															<button class="btn">Upload a file</button>
															<input type="file"name="brand_image" alt="select">
														</div>
													</div>
													<div class="inline-remember-me">
														<input
															class="btn btn-sm btn-primary pull-right login-submit-cs"
															id="brandBtn" type="submit" name="submit"
															value="Add Brand">
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<%
					if(user!=null)
					{
						List<Brand> brands = UserDAO.getInstance().getBrandByUser(user);
						
					%>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                            <div class="single-review-st-hd">
                                <h2>Brand List</h2>
                            </div>
                            <%
                            int flag = 0;
                            for(Brand brand : brands)
                            {
                            	flag = 1;
                            %>
                            <div class="single-review-st-text">
                                <img src="uploads/images/brandImage/<%=brand.getLogo()%>" width="50px" >
                                <div class="review-ctn-hf">
                                    <h3><%=brand.getName() %></h3>
                                    <p><%=brand.getDiscription() %></p>
                                </div>
                                <div class="review-item-rating">
                                    <a href="#" >Edit</a> / <a onclick="return confirm('Do you want to delete Brand(<%=brand.getName() %>)')" href="Catalog?deleteBrand=true&brandId=<%=brand.getId()%>">Delete</a>
                                </div>
                            </div>
                            <%
                            } 
                            if(flag == 0)
                            {
                            	out.print("No Brand found...");
                            }
						}
                            %>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- Basic Form End-->
	<div class="footer-copyright-area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="footer-copy-right">
						<p>
							Copyright � 2019. All rights reserved. Template by <a
								href="https://colorlib.com/wp/templates/">Colorlib</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div id="myProfileImage" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Choose Profile Image</h4>
				</div>
				<form>
					<div class="modal-body">
						<img src="uploads/images/upload.png" id="uploadImage"> <input
							type="file" name="profile" id="uploadFile" style="display: none">

					</div>
					<div class="modal-footer">
						<p class="pull-left" style="color: red" id="uploadErr"></p>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="uploadBtn">Upload</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<!-- jquery
		============================================ -->
	<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<!-- bootstrap JS
		============================================ -->
	<script src="js/bootstrap.min.js"></script>
	<!-- wow JS
		============================================ -->
	<script src="js/wow.min.js"></script>
	<!-- price-slider JS
		============================================ -->
	<script src="js/jquery-price-slider.js"></script>
	<!-- meanmenu JS
		============================================ -->
	<script src="js/jquery.meanmenu.js"></script>
	<!-- owl.carousel JS
		============================================ -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- sticky JS
		============================================ -->
	<script src="js/jquery.sticky.js"></script>
	<!-- scrollUp JS
		============================================ -->
	<script src="js/jquery.scrollUp.min.js"></script>
	<!-- mCustomScrollbar JS
		============================================ -->
	<script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/scrollbar/mCustomScrollbar-active.js"></script>
	<!-- metisMenu JS
		============================================ -->
	<script src="js/metisMenu/metisMenu.min.js"></script>
	<script src="js/metisMenu/metisMenu-active.js"></script>
	<!-- tab JS
		============================================ -->
	<script src="js/tab.js"></script>
	<!-- icheck JS
		============================================ -->
	<script src="js/icheck/icheck.min.js"></script>
	<script src="js/icheck/icheck-active.js"></script>
	<!-- plugins JS
		============================================ -->
	<script src="js/plugins.js"></script>
	<!-- main JS
		============================================ -->
	<script src="js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
	<script src="js/tawk-chat.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#uploadImage").click(function() {
				$("#uploadFile").click();
			});
			$("#uploadBtn").click(function() {
				var file_data = $("#uploadFile").prop("files")[0];
				var form_data = new FormData();
				form_data.append("profile", file_data);
				$.ajax({
					url : "ProfileUpload",
					cache : false,
					contentType : false,
					processData : false,
					data : form_data,
					type : 'post',
					success : function(res) {
						$("#uploadErr").html(res);
						if (res.indexOf("profile uploaded") > -1) {
							location.href = "my-profile.jsp";
						}
					}
				});
			});
			$("#brandName").click(function(){
				$("#errMessage").html("");
			});
		});
	</script>
</body>

</html>