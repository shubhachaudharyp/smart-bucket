<%@page import="com.cebs.beans.User"%>
<%
	User user = null;
	if(!(session!=null && session.getAttribute("user")!=null))
	{
		response.sendRedirect("login.jsp");
	}
	else
	{
		user = (User)session.getAttribute("user");
	}
%>
<div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="index.html"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                <strong><a href="index.html"><img src="img/logo/logosn.png" alt="" /></a></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <li>
                            <a title="Landing Page" href="index.jsp" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Dashboard</span></a>
                        </li>
                        <li>
                            <a class="has-arrow" href="" aria-expanded="false"><span class="educate-icon educate-professor icon-wrap"></span> <span class="mini-click-non">Catalauge</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a title="All Professors" href="add-brand.jsp"><span class="mini-sub-pro">Add/Edit Brand</span></a></li>
                                <li><a title="Add Professor" href="add-category.jsp"><span class="mini-sub-pro">Add/Edit Category</span></a></li>
                                <li><a title="Edit Professor" href="add-subcategory.jsp"><span class="mini-sub-pro">Add/Edit Subcategory</span></a></li>
                                <li><a title="Professor Profile" href="add-product.jsp"><span class="mini-sub-pro">Add/Edit Product</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="all-students.html" aria-expanded="false"><span class="educate-icon educate-student icon-wrap"></span> <span class="mini-click-non">Settings</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a title="All Students" href="add-state.jsp"><span class="mini-sub-pro">Add/Edit State</span></a></li>
                                <li><a title="Add Students" href="add-city.jsp"><span class="mini-sub-pro">Add/Edit City</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a title="Landing Page" href="add-stock.jsp" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Manage Stock</span></a>
                        </li>
                        
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <!-- End Left menu area -->
    