<%@page import="com.cebs.beans.SubCategory"%>
<%@page import="com.cebs.beans.Category"%>
<%@page import="com.cebs.beans.Brand"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Smart Buckets | addBrand</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- favicon
		============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Google Fonts
		============================================ -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900"
	rel="stylesheet">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- owl.carousel CSS
		============================================ -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.transitions.css">
<!-- animate CSS
		============================================ -->
<link rel="stylesheet" href="css/animate.css">
<!-- normalize CSS
		============================================ -->
<link rel="stylesheet" href="css/normalize.css">
<!-- meanmenu icon CSS
		============================================ -->
<link rel="stylesheet" href="css/meanmenu.min.css">
<!-- main CSS
		============================================ -->
<link rel="stylesheet" href="css/main.css">
<!-- educate icon CSS
		============================================ -->
<link rel="stylesheet" href="css/educate-custon-icon.css">
<!-- morrisjs CSS
		============================================ -->
<link rel="stylesheet" href="css/morrisjs/morris.css">
<!-- mCustomScrollbar CSS
		============================================ -->
<link rel="stylesheet"
	href="css/scrollbar/jquery.mCustomScrollbar.min.css">
<!-- metisMenu CSS
		============================================ -->
<link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
<link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
<!-- calendar CSS
		============================================ -->
<link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
<link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
<!-- modals CSS
		============================================ -->
<link rel="stylesheet" href="css/modals.css">
<!-- forms CSS
		============================================ -->
<link rel="stylesheet" href="css/form/all-type-forms.css">
<!-- style CSS
		============================================ -->
<link rel="stylesheet" href="style.css">
<!-- responsive CSS
		============================================ -->
<link rel="stylesheet" href="css/responsive.css">
<!-- modernizr JS
		============================================ -->
<script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

	<%@include file="sidebar.jsp"%>

	<!-- End Left menu area -->
	<!-- Start Welcome area -->
	<div class="all-content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="logo-pro">
						<a href="index.html"><img class="main-logo"
							src="img/logo/logo.png" alt="" /></a>
					</div>
				</div>
			</div>
		</div>
		<%@include file="header.jsp"%>
		<!-- Basic Form Start -->
		<div class="basic-form-area mg-b-15">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="sparkline8-list mt-b-30">
							<div class="sparkline8-hd">
								<div class="main-sparkline8-hd">
									<h1>SubCategory Information</h1>
								</div>
								<div class="main-sparkline8-hd" id="errorMessage">
									<%
										if (request.getParameter("msg") != null) {
											if (request.getParameter("msg").equals("sizeErr")) {
												out.print("<h3 style='color:red'>Image must be less than 20MB and greater than 20KB</h3>");
											} else if (request.getParameter("msg").equals("typeErr")) {
												out.print("<h3 style='color:red'>Image must be JPEG/JPG/PNG/GIF only</h3>");
											} else if (request.getParameter("msg").equals("empty")) {
												out.print("<h3 style='color:red'>Fields are empty</h3>");
											} else if (request.getParameter("msg").equals("succ")) {
												out.print("<h3 style='color:green'>SubCategory Inserted</h3>");
											}

										}
									%>
								</div>
							</div>
							<div class="sparkline8-graph">
								<div class="basic-login-form-ad">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="basic-login-inner">
												<form action="Catalog" method="post"enctype="multipart/form-data">
													<div class="form-group col-lg-12">
														<label>Category Type</label> <select
															name="subBrand_Type" class="form-control">
															<%
																if (user != null) {
																	List<Category> categories = UserDAO.getInstance().getCategoriesByUser(user);
																	int flag = 0;
																	for (Category category : categories) {
																		flag = 1;
															%>
															<option value=<%=category.getName()%>>
																<%=category.getName()%></option>
															<%
																}
																}
															%>
														</select>
													</div>
													<div class="col-lg-12">
														<div class="form-group-inner">
															<label>Enter SubCategory</label> 
															<input type="text" class="form-control" name="subCategory_name" id="subCategoryName" placeholder="Enter Category Name" />
														</div>
													</div>
													<div class="col-lg-12">
														<div class="basic-login-inner">
															<br>
															<h3>Logo</h3>
															<input type="file" name="subcategory_image" alt="select">
														</div>
													</div>
													<div class="inline-remember-me">
														<input
															class="btn btn-sm btn-primary pull-right login-submit-cs"
															type="submit" name="submit" value="Add SubCategory">
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



					<%
						if (user != null) {
							List<SubCategory> subCategories = UserDAO.getInstance().getSubCategoryProductById();
					%>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
							<div class="single-review-st-hd">
								<h2>SubCategory List</h2>
							</div>
							<%
								int flag = 0;
									for (SubCategory subCategory : subCategories) {
										flag = 1;
							%>
							<div class="single-review-st-text">
								<img
									src="uploads/images/subCategoryImage/<%=subCategory.getLogo()%>"
									width="50px">
								<div class="review-ctn-hf">
									<h3><%=subCategory.getName()%></h3>
								</div>
								<div class="review-item-rating">
									<a href="#">Edit</a> / <a
										onclick="return confirm('Do you want to delete Category(<%=subCategory.getName()%>)')"
										href="Catalog?deleteSubCategory=true&subCategoryId=<%=subCategory.getId()%>">Delete</a>
								</div>
							</div>
							<%
								}
									if (flag == 0) {
										out.print("No Brand found...");
									}
								}
							%>
						</div>
					</div>
				</div>
			</div>






		</div>
	</div>
	<!-- Basic Form End-->
	<div class="footer-copyright-area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="footer-copy-right">
						<p>
							Copyright � 2019. All rights reserved. Template by <a
								href="https://colorlib.com/wp/templates/">Colorlib</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div id="myProfileImage" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Choose Profile Image</h4>
				</div>
				<form>
					<div class="modal-body">
						<img src="uploads/images/upload.png" id="uploadImage"> <input
							type="file" name="profile" id="uploadFile" style="display: none">

					</div>
					<div class="modal-footer">
						<p class="pull-left" style="color: red" id="uploadErr"></p>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="uploadBtn">Upload</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<!-- jquery
		============================================ -->
	<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<!-- bootstrap JS
		============================================ -->
	<script src="js/bootstrap.min.js"></script>
	<!-- wow JS
		============================================ -->
	<script src="js/wow.min.js"></script>
	<!-- price-slider JS
		============================================ -->
	<script src="js/jquery-price-slider.js"></script>
	<!-- meanmenu JS
		============================================ -->
	<script src="js/jquery.meanmenu.js"></script>
	<!-- owl.carousel JS
		============================================ -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- sticky JS
		============================================ -->
	<script src="js/jquery.sticky.js"></script>
	<!-- scrollUp JS
		============================================ -->
	<script src="js/jquery.scrollUp.min.js"></script>
	<!-- mCustomScrollbar JS
		============================================ -->
	<script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/scrollbar/mCustomScrollbar-active.js"></script>
	<!-- metisMenu JS
		============================================ -->
	<script src="js/metisMenu/metisMenu.min.js"></script>
	<script src="js/metisMenu/metisMenu-active.js"></script>
	<!-- tab JS
		============================================ -->
	<script src="js/tab.js"></script>
	<!-- icheck JS
		============================================ -->
	<script src="js/icheck/icheck.min.js"></script>
	<script src="js/icheck/icheck-active.js"></script>
	<!-- plugins JS
		============================================ -->
	<script src="js/plugins.js"></script>
	<!-- main JS
		============================================ -->
	<script src="js/main.js"></script>
	<!-- tawk chat JS
		============================================ -->
	<script src="js/tawk-chat.js"></script>
	<script>
	
		$(document).ready(function() {
			$("#subCategoryName").click(function() {
				$("#errorMessage").html("");
			});
		});
	</script>
</body>

</html>