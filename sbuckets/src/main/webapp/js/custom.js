$(document).ready(function(){
	$("#loginBtn").click(function(){
		$(this).attr("disabled","true");
		var uname = $("#username").val();
		var pass = $("#password").val();
		if(uname!="" && pass!="")
		{
			$.post("UserServlet",{"uname":uname,"password":pass,"token":"Login"},function(response){
				$("#errLoginMessage").html(response);
				$("#errLoginMessage").css({"color":"red"});
				if(response.indexOf("login success")>-1)
				{
					$("#errLoginMessage").css({"color":"green"});
					location.href = "index.jsp";
				}
				else if(response.indexOf("activate account")>-1)
				{
					$("#errLoginMessage").css({"color":"orange"});
					location.href = "activate-account.jsp";
				}
				$("#loginBtn").removeAttr("disabled");
			});
		}
		else
		{
			$("#errLoginMessage").html("Empty Email/password");
			$("#errLoginMessage").css({"color":"red"});
			$("#loginBtn").removeAttr("disabled");
		}
	});
	$("#username").click(function(){
		$("#errLoginMessage").html("");
	});
	$("#password").click(function(){
		$("#errLoginMessage").html("");	
	});
});