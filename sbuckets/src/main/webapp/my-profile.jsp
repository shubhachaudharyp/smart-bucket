<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Smart Buckets | User Profile</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/educate-custon-icon.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- modals CSS
		============================================ -->
    <link rel="stylesheet" href="css/modals.css">
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="css/form/all-type-forms.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <%@include file="sidebar.jsp" %>
    
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.html"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="header.jsp" %>
        <!-- Basic Form Start -->
        <div class="basic-form-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline8-list mt-b-30">
                            <div class="sparkline8-hd">
                                <div class="main-sparkline8-hd">
                                    <h1>My Profile Information</h1>
                                </div>
                            </div>
                            <div class="sparkline8-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="basic-login-inner">
                                                <form action="UpdateProfile" method="post">
                                                    <div class="form-group-inner">
                                                        <label>Name</label>
                                                        <input type="text" class="form-control" name="name" value="<% if(user!=null){out.print(user.getFname()); if(user.getMname()!=null && !user.getMname().equals("")){out.print(" "+user.getMname());} if(user.getLname()!=null && !user.getLname().equals("")){out.print(" "+user.getLname());}} %>" placeholder="Enter Name" />
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>Primary Email</label>
                                                        <input type="email" class="form-control" name="pr_email" value="<%if(user!=null){out.print(user.getPr_email());} %>" placeholder="Enter Primary Email" />
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>Secondary Email</label>
                                                        <input type="email" class="form-control" name="sc_email" value="<%if(user!=null){out.print(user.getSc_email());} %>" placeholder="Enter Secondary Email" />
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>Primary Phone</label>
                                                        <input type="text" class="form-control" name="pr_phone" value="<%if(user!=null){out.print(user.getPr_phone());} %>" placeholder="Enter Primary Phone" />
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>Secondary Phone</label>
                                                        <input type="text" class="form-control" name="sc_phone" value="<%if(user!=null){out.print(user.getSc_phone());} %>" placeholder="Enter Secondary Phone" />
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>Gender</label>
                                                        <select class="form-control" name="gender" >
                                                        	<option value="male">Male</option>
                                                        	<option value="female">Female</option>
                                                        	<option value="other">Others</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>User Name</label>
                                                        <input type="text" class="form-control" name="username" value="<%if(user!=null){out.print(user.getUsername());} %>" placeholder="Enter User Name" disabled/>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <label>Password</label>
                                                        <input type="password" class="form-control" name="password" placeholder="password" />
                                                    </div>
                                                    <div class="login-btn-inner">
                                                        <div class="inline-remember-me">
                                                            <input class="btn btn-sm btn-primary pull-right login-submit-cs" type="submit" name="submit" value="Update">
                                                            <label>
															<input type="checkbox" class="i-checks"> Remember me </label>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="basic-login-inner">
                                                <h3>Upload Profile</h3>
                                                <a href="#" data-target="#myProfileImage" data-toggle="modal"><i class="fa fa-camera big-icon" style="font-size:30px; position:absolute; left:170px"></i></a>
                                                <img src="uploads/images/profileImage/<%if(image!=null){out.print(image);} %>" class="img-responsive img-thumbnail img-circle">       
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Basic Form End-->
        <div class="footer-copyright-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="footer-copy-right">
                            <p>Copyright � 2019. All rights reserved. Template by <a href="https://colorlib.com/wp/templates/">Colorlib</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Modal -->
<div id="myProfileImage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Profile Image</h4>
      </div>
      <form>
      	<div class="modal-body">
            <img src="uploads/images/upload.png" id="uploadImage">
        	<input type="file" name="profile" id="uploadFile" style="display:none">
        	
	    </div>
	    <div class="modal-footer">
	    	<p class="pull-left" style="color:red" id="uploadErr"></p>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      	<button type="button" class="btn btn-primary" id="uploadBtn">Upload</button>
	    </div>
	 </form>
    </div>
  </div>
</div>


    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- icheck JS
		============================================ -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/icheck/icheck-active.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
    <!-- tawk chat JS
		============================================ -->
    <script src="js/tawk-chat.js"></script>
   	<script type="text/javascript">
   		$(document).ready(function(){
   			$("#uploadImage").click(function(){
   				$("#uploadFile").click();
   			});
   			$("#uploadBtn").click(function(){
   				var file_data = $("#uploadFile").prop("files")[0];   
   			    var form_data = new FormData();
   			    form_data.append("profile", file_data);
   				$.ajax({
   					url : "ProfileUpload",
   			        cache: false,
   			        contentType: false,
   			        processData: false,
   			        data: form_data,                         
   			        type: 'post',
   			        success: function(res){
   			        	$("#uploadErr").html(res);
   			            if(res.indexOf("profile uploaded")>-1)
   			            {
   			            	location.href="my-profile.jsp";
   			            }
   			        }
   				});
   			});
   		});
   	</script>
</body>

</html>